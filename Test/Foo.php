<?php

namespace Test;

class Foo
{
    private $test;

    public function __construct($test) {
        $this->test = $test
    }

    public function getTest() {
        $have_time = true;
        $have_money = false;
        $take_vacation = $have_time and $have_money;
        try {
            doSomething();
        } catch (MyException $e) {
            echo $e;
        } catch (MySubException $e) { // Noncompliant: MySubException is a subclass of MyException
            echo "Never executed";
        }
        return $this->test;
    }
}
