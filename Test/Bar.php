<?php

namespace Test;

require 'autoload.php';

class Bar
{
    private $foo;

    public function __construct(Foo $foo)
    {
        $this->foo = $foo;
    }

    public function getFoo() {
        return $this->foo;
    }
}

$foo = new Foo(1);
var_dump($foo->getTest());
die();
